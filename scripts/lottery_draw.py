import sys
import re
import math
import random


# https://www.textfixer.com/tools/remove-white-spaces.php

# konstanty
COUNT = 7777
SINGLESET = 1111
SETS = 7
PRICING = [1, 11, 31, 68, 131, 245, 624]

if sum(PRICING) != SINGLESET:
	print('Check pricing: awards != singleset.')

if SETS * SINGLESET != COUNT:
	print("Check the number of sets and count.")

if len(sys.argv) != 2:
	print('Provide how many lines you called the expand func with, for the check.')

try:
	count = int(sys.argv[1])
except:
	print('Provide integer value.')
	exit()

exp = re.compile("[^\S]")

# proved zkraceni na pocet radku na zacatku. nevim kolik jich ma byt takze radsi od konce
with open('copied_numbers') as f:
	
	uncut_lines = [exp.sub('', i) for i in f]
	
# pokud je delka mensi nez co zadal, vyhod chybu
if len(uncut_lines) < count:
	print("Double check, count of numbers is smaller than what you stated.")
	exit()

# preved na dlouhy string
numbers = (str(int(i, base=16)) for i in uncut_lines[len(uncut_lines)-count:])
str_together = "".join(numbers) 

# musi vyjit vic bajtu nez je potreba pro MT, jinak vyhod warning
required_bits = math.ceil(sum(math.log2(i) for i in range(2, SINGLESET)))
required_bytes = math.ceil(required_bits / 8)

if len(str_together) < required_bytes:
	print('Count of strings must be bigger, so generate more of them using expand().')
	exit()

# tohle je seznam seedu ktere jdou pouzit pro kazdy set
seeds = [str_together[i - required_bytes:i] for i in range(required_bytes, len(str_together)+required_bytes,required_bytes)]

# proved generovani pro kazdy set
ids_sada = []
for i in range(0, SETS):
	ids_sada.append([j for j in range(i * SINGLESET + 1, (i+1) * SINGLESET + 1)])

# globalni seznam vyhercu
global_winners = [[] for i in range(0, SETS)]

# zpracovani sady
for x, sada in enumerate(ids_sada):

	# zamichej ids podle seedu teto sady
	random.seed(seeds[x])
	random.shuffle(sada)
	
	# uloz vyherce do globalniho pole, kvuli prirazeni konkretni sady na konci
	c = 0
	for e, group_size in enumerate(PRICING):
		global_winners[e] += sada[c:c+group_size]
		c += group_size

# zamichej seznamy - kvuli prirazeni konkretnich tokenu, u kterych zname dopredu poradi 1 - 7
for e, winner_group in enumerate(global_winners):

	# tenhle seznam zamichej pomoci bajtu vytvorenych z puvodniho seedu
	random.seed(str(seeds[e]).encode())
	random.shuffle(winner_group)

# v tuhle chvili ber seznam tokenu co mas ulozenej oddelene. v adresarich 1 - 7 budou ruzne tokeny, do kazdeho souboru co tam je pridej cislo toho tokenu co mas v seznamu a uloz to. Obrazky co jsou provazane na ty cisla se nachazi v jinem adresari a metadata uz maji pridanou hash v ramci tech souboru co jsou v tech adresarich.
