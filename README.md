# Lucky time walkers NFT

This repo contains description of nft sale process and few technical concepts used in Lucky Time Walkers NFT project (ERC721, Solidity - ETH blockchain). Any comments are welcome.

7777 tokens are split to seven groups of rarity: 7 + 77 + 217 + 476 + 917 + 1717 + 4365. The higher rarity, the higher prices.

## Mint process

Mint process is split to three stages:

1. presale (max 10 tokens / address, mint limit = 10)
2. number of whitelist waves (max 2 tokens / address / 1 wave, mint limit = 2)
3. opensale (no limit / address, mint limit = 10)

## Error codes

These codes are invoked by contract.

| code | meaning |
| ------ | ------ |
| ALL_SOLD | All 7777 tokens has been sold out. |
| SALE_PAUSED | Sale is currently paused. |
| INV_SIGN | ECDSA signature is invalid. |
| INV_NONCE | Nonce has been used already. |
| INV_HASH | Hash doesn't correspond. |
| ZER_ERR | Zero address used (0x0). |
| MINT_2_MANY | Tried to mint too many tokens at once. |
| NFT_LIMIT | Not enough tokens to buy. |
| PHASE_LIMIT | Reached token limit for this specific phase of sale process. |
| ETH_LOW | Not enough ETH. |
| NOT_WL | Address is not whitelisted. |
| MAX_WL_REACHED | internal |
| NO_ETH | internal |
| TX_FAIL | internal |

# Couple of concepts

There are some concepts (I am saving it here to remind me using them for future). Proudly using OpenZeppelin library, very useful. There is also brief guide for validating lottery results.

## ECDSA signing

**What for**: ensuring that minting will be performed only through our web application

**Why**: to keep off bots, nonce prevents replay attacks

```
    mapping(string => bool) private usedNonce;
    address private signaturePubkey;

    function hashIt(address sender, uint256 howmany, string memory nonce) private pure returns(bytes32) {
        bytes32 hash = keccak256(abi.encodePacked(sender, howmany, nonce)); 
        return hash;
    }
    
    function signatureOk(bytes32 hash, bytes memory signature) private view returns(bool) {
        return signaturePubkey == ECDSA.recover(ECDSA.toEthSignedMessageHash(hash), signature);
    }

```

Usage:
```
    require(signatureOk(hash, signature), "INV_SIGN");
    require(!usedNonce[nonce], "INV_NONCE");
    require(hashIt(msg.sender, _howmany, nonce) == hash, "INV_HASH");
```
## Whitelisting using Merkle trees

**What for**: management of long lists of ETH addresses 

**Why**: Effective and cheaper way how to validate membership in set of many elements. Don't understand why some other people store it in arrays so inefficiently.

```
bytes32 private MERKLE;

```
Usage:
```
    bytes32 leaf = keccak256(abi.encodePacked(msg.sender));
    require(MerkleProof.verify(_proof, MERKLE, leaf), "NOT_WL");
```

## Using bit operations for multivalue storage

**What for**: setting of mint limits 

**Why**: Since there is different limit for every sale phase (presale, whitelists, opensale) and number of these phases is limited, instead of saving these values separately it is possible to save all of them to one uint, performing bit operations. Simple bit clear and bit set are used, here every sale phase work with 4 bits (15 tokens max). I haven't been in this world before but it surprised me how many projects could use it and don't (storage = expensive).

```
// 0000 0010 0001 0010 0001 0000 0000 1010 = 0 | 2 | 1 | 2 | 1 | 0 | 0 | 10 
mapping(address => uint32) private tokenLimits;
uint256 public activeWhitelists = 0;

function readBitValue() internal view returns (uint32) {
    return (WL ? uint32(tokenLimits[msg.sender] / (2 ** (activeWhitelists * 4))) : tokenLimits[msg.sender]);
}

```
Usage:
```
uint256 start = (WL ? activeWhitelists * 4 : 0);
for (uint8 c = 0; c < 4; c++) {
    tokenLimits[msg.sender] &= ~(uint32(1) << uint8(start + c));
}
tokenLimits[msg.sender] |= (uint32(_howmany + actualCount) << uint32(start));
```

## Generating random values

**What for**: generate random numbers 

**Why**: I am horrified that some people actually use block numbers etc. to generate random values, often in lottery projects which should rely on reliable source of randomness. We use ChainLink VRF, I split random generation to separate contract (it's address is enlisted in main contract constructor and it is immutable). The reason was the contract size limit and I also wanted to avoid calling contracts from each other.

```
    function getRandomSeed() public onlyOwner returns (bytes32 requestId) {
        require(LINK.balanceOf(address(this)) >= vrfFee, "NO_LINK");
        return requestRandomness(vrfKeyHash, vrfFee);
    }
	
    function fulfillRandomness(bytes32 requestId, uint256 randomness) internal override {
    	emit InitRandomDraw(requestId, randomness);
    	initRandom = randomness;
    }
    
	function expand(uint256 n) public onlyOwner {
    	uint256[] memory expandedValues = new uint256[](n);
    	for (uint256 i = 0; i < n; i++) {
        	expandedValues[i] = uint256(keccak256(abi.encode(initRandom, i)));
    	}
    	emit LotteryDraw(expandedValues);
	}

```
How it works (see the source code or contact me when in doubt):

- random value is generated by VRF
- this value is extended to *x* numbers which are actually needed
- these *x* numbers are used for seeding Mersenne Twister PRNG
- these methods emit events so any user can actually verify that lottery isn't rigged (you can see these events e.g. using Etherscan.io)
- based on random seed, metadata for tokens are shuffled with Fisher-Yates (because of Mersenne Twister we can actually shuffle max. 2080 items, so we split process to 7 sets, see the python script)
- this process is replicable with python3 script, placed in this repo (run the script in python3 virtual environment - see file with instructions)
- this is very simple way how to ensure that lottery results are verifiable by any user, any time

## Provenance hash

**What for**: proving that image representation and order of tokens hasn't been tampered with from beginning

**Why**: There is nice explaining article on topic [external link](https://medium.com/coinmonks/the-elegance-of-the-nft-provenance-hash-solution-823b39f99473), which says:

`Developers need a way to prove that the order of the Token Image and metadata was set pre-mint, and was not manipulated.`

and

`Developers need to generate a fair random number [.. block number of the last minted NFT in the collection, and dividing it by the total supply of the NFT and taking the remainder ..] post-mint to start the distribution of the NFT from that number.`

With shuffling described above **this is not needed**, also I suspect that the random-generation method mentioned in article might be exploitable by malicious player. I still enlisted provenance hash of original sequence of json metadata in order which they have been delivered from illustrator.

Every token json metadata includes hash of connected imaga data and also original sequence. Since we have 7 levels of rarity, the order of rarity is marked as this: `(a-g)(1-1111)`, where first letter means rarity (a = most rare) and number is token index in original order.

Using these two attributes (hash of image data and original sequence), it is possible to prove that when lottery occurs, token metadata + image data are being assigned in predefined order, and not just by will of developer (**since there still might be some kind of uniqueness amongst tokens of same rarity**).

## Where is Safemath?

**What for**: checking overflow

**Why**: don't need anymore, from 0.8 arithmetic operations revert on underflow and overflow.
